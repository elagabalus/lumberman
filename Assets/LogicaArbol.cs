using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaArbol : MonoBehaviour
{
    public float vidaObjeto;
    public float danio;
    float vida;
    // Start is called before the first frame update
    void Start()
    {
        vida = vidaObjeto;
    }

    // Update is called once per frame
    void Update()
    {
        if (vida <= 0)
        {
            Destroy(gameObject,1);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("AtaqueJugador"))
        {
            Debug.Log("golpe al arbol");
            vida -= danio;
        }
    }
}
